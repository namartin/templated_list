// Build with 
//		g++ main.cpp library/Person.h
// Run with 
// 		./a.out

#include <cstdlib>
#include <string>
#include "library/TList.h"
#include "library/Person.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;


static int testInts();
static int testDbls();
static int testStr();
static int testPerson();


int main(int argc, char** argv)
{
    testInts();
    testDbls();
	testStr();
	testPerson();
    
    return 0;
}

int testInts()
{
    TList<int32_t>* list;
    
    TList<int32_t>::node* head;
    TList<int32_t>::node* tail;
    int32_t size;
    int32_t append_retval;
    int32_t insert_retval;
    int32_t delete_retval;
    TList<int32_t>::node* node;
    
    list = new TList<int32_t>();
    
    // empty list
    head = list->findHead(); // should point to 0
    tail = list->findTail(); // should point to 0
    size = list->getSize(); // should be 0
    delete_retval = list->deleteNode(0); // empty list so no changes made; delete_retval = -1
    list->displayList(); // "Empty list\n"
    
    // append temporary node at end (index 0)
    append_retval = list->appendNode(3); // should be 0
    head = list->findHead(); // data = 3
    tail = list->findTail(); // data = 3
    size = list->getSize(); // should be 1
    list->displayList(); // 3
    
    // delete temporary node (index 0)
    delete_retval = list->deleteNode(0); // should be 0
    head = list->findHead(); // should point to 0
    tail = list->findTail(); // should point to 0
    size = list->getSize(); // should be 0
    list->displayList(); // "Empty list\n"
    
    // append node at end (index 0)
    append_retval = list->appendNode(10); // should be 0
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 10
    size = list->getSize(); // should be 1
    list->displayList(); // 10
    
    // append node at end (index 1)
    append_retval = list->appendNode(20); // should be 0
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 20
    size = list->getSize(); // should be 2
    list->displayList(); // 10 20
    
    // append node at end (index 2)
    append_retval = list->appendNode(30); // should be 0
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 30
    size = list->getSize(); // should be 3
    list->displayList(); // 10 20 30
    
     // insert nodes at invalid indices -- no changes made; retval = -1
    insert_retval = list->insertNode(-5, 50); // should be -1
    insert_retval = list->insertNode(10, 60); // should be -1
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 30
    size = list->getSize(); // should be 3
    list->displayList(); // 10 20 30
    
    // insert node at index 0
    insert_retval = list->insertNode(0, 5); // should be 0
    head = list->findHead(); // data = 5
    tail = list->findTail(); // data = 30
    size = list->getSize(); // should be 4
    list->displayList(); // 5 10 20 30
    
    // insert node at index 2
    insert_retval = list->insertNode(2, 15); // should be 0
    head = list->findHead(); // data = 5
    tail = list->findTail(); // data = 30
    size = list->getSize(); // should be 5
    list->displayList(); // 5 10 15 20 30
    
    // insert node at index 5 (end)
    insert_retval = list->insertNode(5, 40); // should be 0
    head = list->findHead(); // data = 5
    tail = list->findTail(); // data = 40
    size = list->getSize(); // should be 6
    list->displayList(); // 5 10 15 20 30 40
    
    // test the getNode() method
    node = list->getNode(-1); // index too low; node = NULL
    node = list->getNode(6); // index too high; node = NULL
    node = list->getNode(0); // data = 5
    node = list->getNode(1); // data = 10
    node = list->getNode(2); // data = 15
    node = list->getNode(-1); // index too low; node = NULL
    node = list->getNode(3); // data = 20
    node = list->getNode(4); // data = 30
    node = list->getNode(5); // data = 40
    node = list->getNode(10); // index too high; node = NULL
    
    // delete nodes at invalid indices -- no changes made; retval = -1
    delete_retval = list->deleteNode(-5); // should be -1
    delete_retval = list->deleteNode(6); // should be -1
    head = list->findHead(); // data = 5
    tail = list->findTail(); // data = 40
    size = list->getSize(); // should be 6
    list->displayList(); // 5 10 15 20 30 40
    
    // delete node at index 0
    delete_retval = list->deleteNode(0); // should be 0
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 40
    size = list->getSize(); // should be 5
    list->displayList(); // 10 15 20 30 40
    
    // delete node at index 2
    delete_retval = list->deleteNode(2); // should be 0
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 40
    size = list->getSize(); // should be 4
    list->displayList(); // 10 15 30 40
    
    // delete node at end (index 3)
    delete_retval = list->deleteNode(3); // should be 0
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 30
    size = list->getSize(); // should be 3
    list->displayList(); // 10 15 30
    
    // delete node at index 1
    delete_retval = list->deleteNode(1); // should be 0
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 30
    size = list->getSize(); // should be 2
    list->displayList(); // 10 30
    
    // delete node at end (index 1)
    delete_retval = list->deleteNode(1); // should be 0
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 10
    size = list->getSize(); // should be 1
    list->displayList(); // 10
    
    // delete node at beginning (index 0)
    delete_retval = list->deleteNode(0); // should be 0
    head = list->findHead(); // should point to 0
    tail = list->findTail(); // should point to 0
    size = list->getSize(); // should be 0
    list->displayList(); // "Empty list\n"
    
    // delete nodes from empty list -- no changes made; retval = -1
    delete_retval = list->deleteNode(0); // should be -1
    head = list->findHead(); // should point to 0
    tail = list->findTail(); // should point to 0
    size = list->getSize(); // should be 0
    list->displayList(); // "Empty list\n"
    
    list->appendNode(5);
    list->appendNode(6);
    list->appendNode(7);
    list->appendNode(8);

    delete list;
	list = 0;
    
    return 0;
}

int testDbls()
{
    TList<double>* list;
    
    TList<double>::node* head;
    TList<double>::node* tail;
    int32_t size;
    int32_t append_retval;
    int32_t insert_retval;
    int32_t delete_retval;
    TList<double>::node* node;
    
    list = new TList<double>();
    
    // empty list
    head = list->findHead(); // should point to 0
    tail = list->findTail(); // should point to 0
    size = list->getSize(); // should be 0
    delete_retval = list->deleteNode(0); // empty list so no changes made; delete_retval = -1
    list->displayList(); // "Empty list\n"
    
    // append temporary node at end (index 0)
    append_retval = list->appendNode(3.5); // should be 0
    head = list->findHead(); // data = 3.5
    tail = list->findTail(); // data = 3.5
    size = list->getSize(); // should be 1
    list->displayList(); // 3.5
    
    // delete temporary node (index 0)
    delete_retval = list->deleteNode(0); // should be 0
    head = list->findHead(); // should point to 0
    tail = list->findTail(); // should point to 0
    size = list->getSize(); // should be 0
    list->displayList(); // "Empty list\n"
    
    // append node at end (index 0)
    append_retval = list->appendNode(10.5); // should be 0
    head = list->findHead(); // data = 10.5
    tail = list->findTail(); // data = 10.5
    size = list->getSize(); // should be 1
    list->displayList(); // 10.5
    
    // append node at end (index 1)
    append_retval = list->appendNode(20.5); // should be 0
    head = list->findHead(); // data = 10.5
    tail = list->findTail(); // data = 20.5
    size = list->getSize(); // should be 2
    list->displayList(); // 10.5 20.5
    
    // append node at end (index 2)
    append_retval = list->appendNode(30.5); // should be 0
    head = list->findHead(); // data = 10.5
    tail = list->findTail(); // data = 30.5
    size = list->getSize(); // should be 3
    list->displayList(); // 10.5 20.5 30.5
    
     // insert nodes at invalid indices -- no changes made; retval = -1
    insert_retval = list->insertNode(-5, 50.5); // should be -1
    insert_retval = list->insertNode(10, 60.5); // should be -1
    head = list->findHead(); // data = 10.5
    tail = list->findTail(); // data = 30.5
    size = list->getSize(); // should be 3
    list->displayList(); // 10.5 20.5 30.5
    
    // insert node at index 0
    insert_retval = list->insertNode(0, 5.5); // should be 0
    head = list->findHead(); // data = 5.5
    tail = list->findTail(); // data = 30.5
    size = list->getSize(); // should be 4
    list->displayList(); // 5.5 10.5 20.5 30.5
    
    // insert node at index 2
    insert_retval = list->insertNode(2, 15.5); // should be 0
    head = list->findHead(); // data = 5.5
    tail = list->findTail(); // data = 30.5
    size = list->getSize(); // should be 5
    list->displayList(); // 5.5 10.5 15.5 20.5 30.5
    
    // insert node at index 5 (end)
    insert_retval = list->insertNode(5, 40.5); // should be 0
    head = list->findHead(); // data = 5.5
    tail = list->findTail(); // data = 40.5
    size = list->getSize(); // should be 6
    list->displayList(); // 5.5 10.5 15.5 20.5 30.5 40.5
    
    // test the getNode() method
    node = list->getNode(-1); // index too low; node = NULL
    node = list->getNode(6); // index too high; node = NULL
    node = list->getNode(0); // data = 5.5
    node = list->getNode(1); // data = 10.5
    node = list->getNode(2); // data = 15.5
    node = list->getNode(-1); // index too low; node = NULL
    node = list->getNode(3); // data = 20.5
    node = list->getNode(4); // data = 30.5
    node = list->getNode(5); // data = 40.5
            
    node = list->getNode(10); // index too high; node = NULL
    
    // delete nodes at invalid indices -- no changes made; retval = -1
    delete_retval = list->deleteNode(-5); // should be -1
    delete_retval = list->deleteNode(6); // should be -1
    head = list->findHead(); // data = 5.5
    tail = list->findTail(); // data = 40.5
    size = list->getSize(); // should be 6
    list->displayList(); // 5.5 10.5 15.5 20.5 30.5 40.5
    
    // delete node at index 0
    delete_retval = list->deleteNode(0); // should be 0
    head = list->findHead(); // data = 10.5
    tail = list->findTail(); // data = 40.5
    size = list->getSize(); // should be 5
    list->displayList(); // 10.5 15.5 20.5 30.5 40.5
    
    // delete node at index 2
    delete_retval = list->deleteNode(2); // should be 0
    head = list->findHead(); // data = 10.5
    tail = list->findTail(); // data = 40.5
    size = list->getSize(); // should be 4
    list->displayList(); // 10.5 15.5 30.5 40.5
    
    // delete node at end (index 3)
    delete_retval = list->deleteNode(3); // should be 0
    head = list->findHead(); // data = 10.5
    tail = list->findTail(); // data = 30.5
    size = list->getSize(); // should be 3
    list->displayList(); // 10.5 15.5 30.5
    
    // delete node at index 1
    delete_retval = list->deleteNode(1); // should be 0
    head = list->findHead(); // data = 10.5
    tail = list->findTail(); // data = 30.5
    size = list->getSize(); // should be 2
    list->displayList(); // 10.5 30.5
    
    // delete node at end (index 1)
    delete_retval = list->deleteNode(1); // should be 0
    head = list->findHead(); // data = 10.5
    tail = list->findTail(); // data = 10.5
    size = list->getSize(); // should be 1
    list->displayList(); // 10.5
    
    // delete node at beginning (index 0)
    delete_retval = list->deleteNode(0); // should be 0
    head = list->findHead(); // should point to 0
    tail = list->findTail(); // should point to 0
    size = list->getSize(); // should be 0
    list->displayList(); // "Empty list\n"
    
    // delete nodes from empty list -- no changes made; retval = -1
    delete_retval = list->deleteNode(0); // should be -1
    head = list->findHead(); // should point to 0
    tail = list->findTail(); // should point to 0
    size = list->getSize(); // should be 0
    list->displayList(); // "Empty list\n"
    
    list->appendNode(5);
    list->appendNode(6);
    list->appendNode(7);
    list->appendNode(8);

    delete list;
	list = 0;
    
    return 0;
}


int testStr()
{
    TList<string>* list;
    
    TList<string>::node* head;
    TList<string>::node* tail;
    int32_t size;
    int32_t append_retval;
    int32_t insert_retval;
    int32_t delete_retval;
    TList<string>::node* node;
    
    list = new TList<string>();
    
    // empty list
    head = list->findHead(); // should point to 0
    tail = list->findTail(); // should point to 0
    size = list->getSize(); // should be 0
    delete_retval = list->deleteNode(0); // empty list so no changes made; delete_retval = -1
    list->displayList(); // "Empty list\n"
    
    // append temporary node at end (index 0)
    append_retval = list->appendNode("three"); // should be 0
    head = list->findHead(); // data = 3
    tail = list->findTail(); // data = 3
    size = list->getSize(); // should be 1
    list->displayList(); // 3
    
    // delete temporary node (index 0)
    delete_retval = list->deleteNode(0); // should be 0
    head = list->findHead(); // should point to 0
    tail = list->findTail(); // should point to 0
    size = list->getSize(); // should be 0
    list->displayList(); // "Empty list\n"
    
    // append node at end (index 0)
    append_retval = list->appendNode("ten"); // should be 0
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 10
    size = list->getSize(); // should be 1
    list->displayList(); // 10
    
    // append node at end (index 1)
    append_retval = list->appendNode("twenty"); // should be 0
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 20
    size = list->getSize(); // should be 2
    list->displayList(); // 10 20
    
    // append node at end (index 2)
    append_retval = list->appendNode("thirty"); // should be 0
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 30
    size = list->getSize(); // should be 3
    list->displayList(); // 10 20 30
    
     // insert nodes at invalid indices -- no changes made; retval = -1
    insert_retval = list->insertNode(-5, "fifty"); // should be -1
    insert_retval = list->insertNode(10, "sixty"); // should be -1
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 30
    size = list->getSize(); // should be 3
    list->displayList(); // 10 20 30
    
    // insert node at index 0
    insert_retval = list->insertNode(0, "five"); // should be 0
    head = list->findHead(); // data = 5
    tail = list->findTail(); // data = 30
    size = list->getSize(); // should be 4
    list->displayList(); // 5 10 20 30
    
    // insert node at index 2
    insert_retval = list->insertNode(2, "fifteen"); // should be 0
    head = list->findHead(); // data = 5
    tail = list->findTail(); // data = 30
    size = list->getSize(); // should be 5
    list->displayList(); // 5 10 15 20 30
    
    // insert node at index 5 (end)
    insert_retval = list->insertNode(5, "forty"); // should be 0
    head = list->findHead(); // data = 5
    tail = list->findTail(); // data = 40
    size = list->getSize(); // should be 6
    list->displayList(); // 5 10 15 20 30 40
    
    // test the getNode() method
    node = list->getNode(-1); // index too low; node = NULL
    node = list->getNode(6); // index too high; node = NULL
    node = list->getNode(0); // data = 5
    node = list->getNode(1); // data = 10
    node = list->getNode(2); // data = 15
    node = list->getNode(-1); // index too low; node = NULL
    node = list->getNode(3); // data = 20
    node = list->getNode(4); // data = 30
    node = list->getNode(5); // data = 40
    node = list->getNode(10); // index too high; node = NULL
    
    // delete nodes at invalid indices -- no changes made; retval = -1
    delete_retval = list->deleteNode(-5); // should be -1
    delete_retval = list->deleteNode(6); // should be -1
    head = list->findHead(); // data = 5
    tail = list->findTail(); // data = 40
    size = list->getSize(); // should be 6
    list->displayList(); // 5 10 15 20 30 40
    
    // delete node at index 0
    delete_retval = list->deleteNode(0); // should be 0
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 40
    size = list->getSize(); // should be 5
    list->displayList(); // 10 15 20 30 40
    
    // delete node at index 2
    delete_retval = list->deleteNode(2); // should be 0
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 40
    size = list->getSize(); // should be 4
    list->displayList(); // 10 15 30 40
    
    // delete node at end (index 3)
    delete_retval = list->deleteNode(3); // should be 0
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 30
    size = list->getSize(); // should be 3
    list->displayList(); // 10 15 30
    
    // delete node at index 1
    delete_retval = list->deleteNode(1); // should be 0
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 30
    size = list->getSize(); // should be 2
    list->displayList(); // 10 30
    
    // delete node at end (index 1)
    delete_retval = list->deleteNode(1); // should be 0
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 10
    size = list->getSize(); // should be 1
    list->displayList(); // 10
    
    // delete node at beginning (index 0)
    delete_retval = list->deleteNode(0); // should be 0
    head = list->findHead(); // should point to 0
    tail = list->findTail(); // should point to 0
    size = list->getSize(); // should be 0
    list->displayList(); // "Empty list\n"
    
    // delete nodes from empty list -- no changes made; retval = -1
    delete_retval = list->deleteNode(0); // should be -1
    head = list->findHead(); // should point to 0
    tail = list->findTail(); // should point to 0
    size = list->getSize(); // should be 0
    list->displayList(); // "Empty list\n"
    
    list->appendNode("five");
    list->appendNode("six");
    list->appendNode("seven");
    list->appendNode("eight");

    delete list;
	list = 0;
    
    return 0;
}


int testPerson()
{
	//Person test("Nick", "Martin", 35);
	//cout << test;
	
    TList<Person>* list;
    
    TList<Person>::node* head;
    TList<Person>::node* tail;
    int32_t size;
    int32_t append_retval;
    int32_t insert_retval;
    int32_t delete_retval;
    TList<Person>::node* node;
    
    list = new TList<Person>();
    
    // empty list
    head = list->findHead(); // should point to 0
    tail = list->findTail(); // should point to 0
    size = list->getSize(); // should be 0
    delete_retval = list->deleteNode(0); // empty list so no changes made; delete_retval = -1
    list->displayList(); // "Empty list\n"
	
    // append temporary node at end (index 0)
	Person temp_person("Temp", "Person", 100);
	//cout << "Trying to add: " << temp_person << endl;
    append_retval = list->appendNode(temp_person); // should be 0
	//cout << "append_retval = " << append_retval << endl;
    head = list->findHead();
    tail = list->findTail();
    size = list->getSize(); // should be 1
	//cout << "size = " << size << endl;
    list->displayList(); // Temp Person 100

	//Person blah = list->findHead()->data;
	//cout << blah << endl;


    
    // delete temporary node (index 0)
    delete_retval = list->deleteNode(0); // should be 0
    head = list->findHead(); // should point to 0
    tail = list->findTail(); // should point to 0
    size = list->getSize(); // should be 0
    list->displayList(); // "Empty list\n"
    
    // append node at end (index 0)
    append_retval = list->appendNode(Person("Nick", "Martin", 35)); // should be 0
    head = list->findHead();
    tail = list->findTail();
    size = list->getSize(); // should be 1
    list->displayList(); // Nick Martin 35
    
    // append node at end (index 1)
    append_retval = list->appendNode(Person("Rocio", "Martin", 40)); // should be 0
    head = list->findHead();
    tail = list->findTail();
    size = list->getSize(); // should be 2
    list->displayList(); // Nick Martin 35; Rocio Martin 40
    
    // append node at end (index 2)
    append_retval = list->appendNode(Person("Sebastian", "Martin", 13)); // should be 0
    head = list->findHead();
    tail = list->findTail();
    size = list->getSize(); // should be 3
    list->displayList(); // Nick Martin 35; Rocio Martin 40; Sebastian Martin 13
    
     // insert nodes at invalid indices -- no changes made; retval = -1
    insert_retval = list->insertNode(-5, Person("Invalid person", "NEGATIVE", -5)); // should be -1
    insert_retval = list->insertNode(10, Person("Invalid person", "TOO HIGH", 10)); // should be -1
    head = list->findHead();
    tail = list->findTail();
    size = list->getSize(); // should be 3
    list->displayList(); // Nick Martin 35; Rocio Martin 40; Sebastian Martin 13
    
    // insert node at index 0
    insert_retval = list->insertNode(0, Person("Valerie", "Martin", 9)); // should be 0
    head = list->findHead();
    tail = list->findTail();
    size = list->getSize(); // should be 4
    list->displayList(); // Valerie Martin 9; Nick Martin 35; Rocio Martin 40; Sebastian Martin 13
    
    // insert node at index 2
    insert_retval = list->insertNode(2, Person("Calvin (Palenka)", "Martin", 1)); // should be 0
    head = list->findHead();
    tail = list->findTail();
    size = list->getSize(); // should be 5
    list->displayList(); // Valerie Martin 9; Nick Martin 35; Calvin (Palenka) Martin 1; Rocio Martin 40; Sebastian Martin 13
    
    // insert node at index 5 (end)
    insert_retval = list->insertNode(5, Person("Jason", "Martin", 11)); // should be 0
    head = list->findHead();
    tail = list->findTail();
    size = list->getSize(); // should be 6
    list->displayList(); // Valerie Martin 9; Nick Martin 35; Calvin (Palenka) Martin 1; Rocio Martin 40; Sebastian Martin 13; Jason Martin 11
    
    // test the getNode() method
    node = list->getNode(-1); // index too low; node = NULL
    node = list->getNode(6); // index too high; node = NULL
    node = list->getNode(0);
    node = list->getNode(1);
    node = list->getNode(2);
    node = list->getNode(-1); // index too low; node = NULL
    node = list->getNode(3);
    node = list->getNode(4);
    node = list->getNode(5);
    node = list->getNode(10); // index too high; node = NULL
    
    // delete nodes at invalid indices -- no changes made; retval = -1
    delete_retval = list->deleteNode(-5); // should be -1
    delete_retval = list->deleteNode(6); // should be -1
    head = list->findHead();
    tail = list->findTail();
    size = list->getSize(); // should be 6
    list->displayList(); // Valerie Martin 9; Nick Martin 35; Calvin (Palenka) Martin 1; Rocio Martin 40; Sebastian Martin 13; Jason Martin 11
    
    // delete node at index 0
    delete_retval = list->deleteNode(0); // should be 0
    head = list->findHead();
    tail = list->findTail();
    size = list->getSize(); // should be 5
    list->displayList(); // Nick Martin 35; Calvin (Palenka) Martin 1; Rocio Martin 40; Sebastian Martin 13; Jason Martin 11
    
    // delete node at index 2
    delete_retval = list->deleteNode(2); // should be 0
    head = list->findHead();
    tail = list->findTail();
    size = list->getSize(); // should be 4
    list->displayList(); // Nick Martin 35; Calvin (Palenka) Martin 1; Sebastian Martin 13; Jason Martin 11
    
    // delete node at end (index 3)
    delete_retval = list->deleteNode(3); // should be 0
    head = list->findHead();
    tail = list->findTail();
    size = list->getSize(); // should be 3
    list->displayList(); // Nick Martin 35; Calvin (Palenka) Martin 1; Sebastian Martin 13
    
    // delete node at index 1
    delete_retval = list->deleteNode(1); // should be 0
    head = list->findHead();
    tail = list->findTail();
    size = list->getSize(); // should be 2
    list->displayList(); // Nick Martin 35; Sebastian Martin 13
    
    // delete node at end (index 1)
    delete_retval = list->deleteNode(1); // should be 0
    head = list->findHead();
    tail = list->findTail();
    size = list->getSize(); // should be 1
    list->displayList(); // Nick Martin 35
    
    // delete node at beginning (index 0)
    delete_retval = list->deleteNode(0); // should be 0
    head = list->findHead();
    tail = list->findTail();
    size = list->getSize(); // should be 0
    list->displayList(); // "Empty list\n"
    
    // delete nodes from empty list -- no changes made; retval = -1
    delete_retval = list->deleteNode(0); // should be -1
    head = list->findHead();
    tail = list->findTail();
    size = list->getSize(); // should be 0
    list->displayList(); // "Empty list\n"
    
    list->appendNode(Person("Joey", "McThrowey", 45));
    list->appendNode(Person("Baddy", "McEvil", 25));
    list->appendNode(Person("Spikey", "Mikey", 20));
    list->appendNode(Person("Tubby", "McFats", 32));

    delete list;
	list = 0;
	
    return 0;
}

