#include "Person.h"
//#include <inttypes.h>
//#include <iostream>    // std::cout
#include <ostream>
#include <string>
//#include <boost/lexical_cast.hpp>
//#include <vector>
#include <sstream>

using namespace std;


Person::Person()
: m_first_name("")
, m_last_name("")
, m_age()
{
}

Person::Person(string first_name)
: m_first_name(first_name)
, m_last_name("")
, m_age()
{
}

Person::Person(string first_name, string last_name)
: m_first_name(first_name)
, m_last_name(last_name)
, m_age()
{
}

Person::Person(string first_name, string last_name, int age)
: m_first_name(first_name)
, m_last_name(last_name)
, m_age(age)
{
}

Person::Person(const Person& orig)
{
	m_first_name = orig.m_first_name;
	m_last_name = orig.m_last_name;
	m_age = orig.m_age;
}

Person::~Person()
{
	// TODO: possibly delete pointer to "m_first_name" and "m_last_name" and set pointers to 0
}

int Person::set_name(string first_name, string last_name)
{
	m_first_name = first_name;
	m_last_name = last_name;
}

int Person::set_first_name(string first_name)
{
	m_first_name = first_name;
}

string Person::get_first_name()
{
	return m_first_name;
}

int Person::set_last_name(string last_name)
{
	m_last_name = last_name;
}

string Person::get_last_name()
{
	return m_last_name;
}

int Person::set_age(int age)
{
	m_age = age;
}

int Person::get_age()
{
	return m_age;
}

/*
string Person::toString() // TODO: replace this with << overload.  See https://stackoverflow.com/questions/1549930/c-equivalent-of-java-tostring, https://stackoverflow.com/questions/27269423/c-what-is-wrong-with-using-a-tostring-method
{
	string retval;
	
	std::ostringstream ss;
	ss << m_first_name + " " + m_last_name + " ";
	ss << m_age;
	retval = ss.str();

	return retval;
}
*/

ostream& operator<<(ostream& os, const Person& dt) 
{
    os << dt.m_first_name + " " + dt.m_last_name + " ";
	os << dt.m_age << endl;

	return os;
}
