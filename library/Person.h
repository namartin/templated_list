#ifndef PERSON_H
#define PERSON_H

//#include <stdio.h>
//#include <stdlib.h>

#include <iostream>
#include <ostream>
#include <cstdlib>
#include <string>

using namespace std;




class Person 
{
public:
    /// Constructors
    Person();
	Person(string first_name);
	Person(string first_name, string last_name);
	Person(string first_name, string last_name, int age);
    
    /// Copy constructor
    Person(const Person& orig);
    
    /// Destructor
    virtual ~Person();

	int set_name(string first_name, string last_name);

	int set_first_name(string first_name);
	string get_first_name();

	int set_last_name(string last_name);
	string get_last_name();

	int set_age(int age);
	int get_age();

	//string toString();

private:
	string m_first_name;
	string m_last_name;
	int m_age;

	friend ostream& operator<<(ostream& os, const Person& dt); // defined in Person.cpp.  This redefines the << operator and is essentially the equivalent of a toString() method
};

#endif /* PERSON_H */



