#ifndef TLIST_H
#define TLIST_H

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;


template <class T>
class TList 
{
public:
    struct node
    {
      T data; // = new T();
      node* ptr_next;
      node* ptr_prev;
      
      ~node();
    };
    
private:
    node* head;
    node* tail;
    int32_t size;

public:
    /// Constructor
    TList();
    
    /// Copy constructor
    TList(const TList& orig);
    
    /// Destructor
    virtual ~TList();
//    ~TList();
    
    /// Append a node to the end of the list
    /// 
    /// \param value         Value of the node's data
    ///
    /// \return 0 if successful; else nonzero
    int32_t appendNode(T value);
    
    /// Insert a node at the given index
    /// 
    /// \param index         Index where to insert the node
    /// \param value         Value for the node's data
    ///
    /// \return 0 if successful; else nonzero
    int32_t insertNode(int32_t index, T value);
    
    /// Delete node at the index
    /// 
    /// \param index         Index of the node to be deleted
    ///
    /// \return 0 if successful; else nonzero
    int32_t deleteNode(int32_t index);
    
    /// Return the total number of nodes
    /// 
    /// \return 
    int32_t getSize() const;
    
    /// Print list to console
    void displayList() const;
    
    node* findHead() const;
    
    node* findTail() const;
    
    /// Get node at a given index
    /// 
    /// \param index         Index of the node you want
    ///
    /// \return a pointer to the node at the given index
    node* getNode(int32_t index) const;
};


template <class T>
TList<T>::TList()
: head(0)
, tail(0)
, size(0)
{
}

template <class T>
TList<T>::TList(const TList& orig)
{
}

template <class T>
TList<T>::~TList()
{
    // delete each node
    int32_t orig_size = size;
    for(int32_t i = 0; i < orig_size; i++)
    {
        deleteNode(0); // keep deleting the head
    }
}

template <class T>
TList<T>::node::~node()
{
	//T* data_ptr = &data;
	//delete data_ptr;
	//data_ptr = 0;
}

template <class T>
int32_t TList<T>::appendNode(T value)
{
	//cout << "appendNode() -- Trying to append: " << value << endl; // TODO: delete this line
    return insertNode(size, value); // insert node at the end
}

template <class T>
int32_t TList<T>::insertNode(int32_t index, T value) // TODO: make "index" be unsigned so I don't need to check for negative
{
	//cout << "insertNode() -- Trying to insert at index " << index << ": " << value << endl; // TODO: delete this line

    if(index < 0 || index > size) // index out of bounds
    {
        return -1;
    }
    
    node* temp = new node;
    temp->data = value;
    temp->ptr_next = 0;
    temp->ptr_prev = 0;
    
    if(!size) // empty list -- the only index possible to get here is 0
    {
        head = temp;
        tail = temp;
    }
    else if(index == 0) // insert at beginning
    {
        temp->ptr_next = head;
        head->ptr_prev = temp;
        head = temp;
    }
    else if(index == size) // insert at end
    {
        temp->ptr_prev = tail; // attach this at the end
        tail->ptr_next = temp;
        tail = temp;      // move the tail
    }
    else // insert somewhere in the middle
    {
        node* prev_node = getNode(index - 1);
        node* next_node = getNode(index);
        
        temp->ptr_next = next_node;
        next_node->ptr_prev = temp;
        
        temp->ptr_prev = prev_node;
        prev_node->ptr_next = temp;
    }
    
    size++;
    
    return 0;
}

template <class T>
int32_t TList<T>::deleteNode(int32_t index) // TODO: make "index" be unsigned so I don't need to check for negative
{
    if(!size || index < 0 || index >= size) // empty list or index out of bounds
    {
        return -1;
    }
    
    node* node_to_delete = getNode(index);
    node* prev_node = 0;
    node* next_node = 0;
    
    if(size == 1)
    {
        head = 0;
        tail = 0;
        delete node_to_delete;
        size--;
        
        return 0;
    }
    
    // if made it this far then list has at least 2 nodes
    
    if(index > 0) // not first node -- get previous node
    {
        prev_node = getNode(index - 1);
    }
    if(index < (size - 1)); // not last node -- get next node
    {
        next_node = getNode(index + 1);
    }
    
    if(index == 0) // delete 1st node -- move head
    {
        next_node->ptr_prev = 0;
        head = next_node;
    }
    else if(index == (size - 1)) // delete last node -- move tail
    {
        prev_node->ptr_next = 0;
        tail = prev_node;
    }
    else // delete node somewhere in the middle
    {
        prev_node->ptr_next = next_node;
        next_node->ptr_prev = prev_node;
    }
        
    delete node_to_delete;
    size--;
    
    return 0;
}

template <class T>
int32_t TList<T>::getSize() const 
{
    return size;
}

template <class T>
void TList<T>::displayList() const
{
    printf("\n"); // blank space before for readability
    
    if(!size)
    {
        printf("Empty list\n");
    }
    else
    {   
        node* curr_node;
        for(int32_t i = 0; i < size; i++)
        {
            if(i == 0)
            {
                curr_node = head;
            }
            else
            {
                curr_node = curr_node->ptr_next;
            }
            
            //printf("%i\t", (int)(curr_node->data)); // this must be the source of the Valgrind errors for the non-templated versions of this program
			cout << curr_node->data << "\t";
        }
    }
    
    printf("\n"); // blank space after for readability
}

template <class T>
typename TList<T>::node* TList<T>::getNode(int32_t index) const // TODO: make "index" be unsigned so I don't need to check for negative
{
    if(index < 0 || index >= size) // index out of bounds
    {
        return NULL;
    }
    
//    if(!head) // empty list -- unreachable since its size == 0 so it will always go into the if statement above
//    {
//        return NULL;
//    }
    
    node* curr_node;
    for(int32_t i = 0; i <= index; i++)
    {
        if(i == 0)
        {
            curr_node = head;
        }
        else
        {
            curr_node = curr_node->ptr_next;
        }
    }
    
    return curr_node;
}

template <class T>
typename TList<T>::node* TList<T>::findHead() const
{
    return head;
}

template <class T>
typename TList<T>::node* TList<T>::findTail() const
{
    return tail;
}

#endif /* TLIST_H */
